// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FPWidget.generated.h"

/**
 * 
 */
UCLASS(Transient, Blueprintable, BlueprintType)
class GAM312_FP_API UFPWidget : public UUserWidget
{
	GENERATED_BODY()


protected:
	virtual void NativeOnInitialized() override;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	float HealthBar{100};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	float MagicBar{100};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int32 Scores{0};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int32 KillCount{0};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	float ProgressionBar{0};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int TimerMins{0};
	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	float TimerSecs{0};

private:
	UFUNCTION()
	void OnPlayerUpdate(float health, float magic);

	UFUNCTION()
	void OnScoreUpdate(int32 Score, int32 NumberOfKill);

	UFUNCTION()
	void OnTimeUpdate(float TimeInSecs);

	UPROPERTY(Transient)
	class AGAM312_FPCharacter* Player;

	UPROPERTY(Transient)
	class AGAM312_FPGameMode* GameMode;

};
