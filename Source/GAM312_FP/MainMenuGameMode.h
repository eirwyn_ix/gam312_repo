// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_FP_API AMainMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Generic", Meta = (BlueprintProtected = "true"))
	TSubclassOf<UUserWidget> StartMenuWidgetClass;

	virtual void BeginPlay() override;
};
