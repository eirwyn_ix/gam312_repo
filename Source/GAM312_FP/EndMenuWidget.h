// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EndMenuWidget.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class GAM312_FP_API UEndMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	// VARIABLES
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* MainMenuBtn;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* QuitBtn;

	UPROPERTY(Transient)
	class AGAM312_FPCharacter* Player;

	UPROPERTY(Transient)
	APlayerController* PlayerController;

	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int32 Score;

	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int TimeSecs;

	UPROPERTY(BlueprintReadOnly, Category = "Generic")
	int TimeMins;

	virtual void NativeConstruct() override;

	virtual void NativeOnInitialized() override;

	UFUNCTION()
	void OnMainMenuClick();

	UFUNCTION()
	void OnQuitClick();
	
};
