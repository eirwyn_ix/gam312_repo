// Fill out your copyright notice in the Description page of Project Settings.


#include "AIAttackAnimNotifyState.h"

#define DEBUG(Cd, F) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cd, F)

void UAIAttackAnimNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float TotalDuration)
{
	Super::NotifyBegin(MeshComp, Animation, TotalDuration);
	DEBUG(Red, __FUNCTION__); // for debugging purpose.
}

void UAIAttackAnimNotifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
	float FrameDeltaTime)
{
	Super::NotifyTick(MeshComp, Animation, FrameDeltaTime);
	DEBUG(Green, __FUNCTION__); // for debugging purpose.
}

void UAIAttackAnimNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::NotifyEnd(MeshComp, Animation);
	DEBUG(Blue, __FUNCTION__); // for debugging purpose.
}
