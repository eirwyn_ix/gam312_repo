// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuGameMode.h"

#include "GAM312_FPCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

void AMainMenuGameMode::BeginPlay()
{
	Super::BeginPlay();
	if(IsValid(StartMenuWidgetClass))
	{
		const auto Widget = CreateWidget(GetWorld(), StartMenuWidgetClass); // create a widget using the class we defined in the editor.
		if(Widget)
		{
			Widget->AddToViewport(); // displaying HUD.
			UGameplayStatics::SetGamePaused(GetWorld(), true); // Pause Game.
			const auto Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			const auto Controller = Player->GetController(); // Displaying Mouse Cursor.
			if (IsValid(Controller))
			{
				APlayerController* PC = Cast<APlayerController>(Controller);
				PC->bShowMouseCursor = true;
				PC->bEnableClickEvents = true;
				PC->bEnableMouseOverEvents = true;
			}
		}
	}
}
