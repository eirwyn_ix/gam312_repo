// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_AnimInstance.h"

#include "AIChar.h"

UAI_AnimInstance::UAI_AnimInstance()
{
}

void UAI_AnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	MyAICharacter = Cast<AAIChar>(GetOwningActor());
	if (IsValid(MyAICharacter))
	{
		speed = MyAICharacter->GetVelocity().Size(); // get velocity length and use it to blend the animation.
		bDied = MyAICharacter->bDied;
		bInAttackRadius = MyAICharacter->bInRadius;
	}
}
