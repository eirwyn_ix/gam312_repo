// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <random>

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

UCLASS()
class GAM312_FP_API ASpawner : public AActor
{
	GENERATED_BODY()

	typedef std::uniform_real_distribution<> URDistr; // declare a type.
	// typedef std::default_random_engine REngine;

public:
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Spawn")
	FVector SpawnBoxExtendInMeters {100, 100, 0};
	UPROPERTY(EditAnywhere, Category = "Spawn")
	TArray<int> SpawnNumbers;
	UPROPERTY(EditAnywhere, Category = "Spawn")
	TArray<TSubclassOf<AActor>> SpawnActors;

private:

	UPROPERTY(Transient)
	UBillboardComponent* Marker; // Billboard component for the ease of locating.


	void ArraySpawn(const int& SpawnNumber, const TSubclassOf<AActor>& SpawnActor, const FVector& ActorLocation) const;
};
