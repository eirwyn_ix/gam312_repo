// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AIWaypoint.generated.h"

UCLASS()
class GAM312_FP_API AAIWaypoint : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAIWaypoint();

	UPROPERTY(EditAnywhere, Transient)
	class ULevelSequence* SEQ;

	UPROPERTY(EditAnywhere, Transient)
	class UMovieScene* MovieScene;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Transient) // Scene Root
	USceneComponent* Root;

	UPROPERTY(Transient) // Overlapping Box
	class UBoxComponent* BoxComponent;

private:

	UFUNCTION() // declare an overlap event for waypoints actor.
	void OnPawnEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	                 int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(Transient)
	class ULevelSequencePlayer* SEQPlayer;

	UPROPERTY(Transient)
	class ALevelSequenceActor* OutActor;

	TArray<FVector> CamPos; 
};
