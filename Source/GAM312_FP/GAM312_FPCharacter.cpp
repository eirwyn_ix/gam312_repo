// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAM312_FPCharacter.h"
#include "DrawDebugHelpers.h"
#include "GAM312_FPGameMode.h"
#include "GAM312_FPProjectile.h"
#include "Animation/AnimInstance.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"

static struct FLogCategoryLogFPChar : public FLogCategory<ELogVerbosity::Warning, ELogVerbosity::All>
{
	FORCEINLINE FLogCategoryLogFPChar() : FLogCategory(TEXT("LogFPChar"))
	{
	}
} LogFPChar;;

AGAM312_FPCharacter::AGAM312_FPCharacter()
{
	// set tick
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = 0.02f;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f); // root component.

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P")); // default mesh component.
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f)); // rotator.
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f)); // the relative location of mesh.

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false); // otherwise won't be visible in the multiplayer
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	// used for spawning projectile.
	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun); // attach to gun.
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f)); // relative to the gun location.

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.
}

void AGAM312_FPCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true),
	                          TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.

	Mesh1P->SetHiddenInGame(false, true);
}

void AGAM312_FPCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CurrentMagic = FMath::FInterpTo(CurrentMagic, 100, DeltaSeconds, .1); // magic regeneration.
	UpdatePlayer(0, 0); // pass through 0 values just for updating hud element.

	SetActorTickEnabled(CurrentMagic < 100); // tick stops when the magic is above 100. (stopping regeneration)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AGAM312_FPCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump); // Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping); // Bind jump events
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AGAM312_FPCharacter::OnFire); // Bind fire events
	PlayerInputComponent->BindAxis("MoveForward", this, &AGAM312_FPCharacter::MoveForward); // Bind movement events
	PlayerInputComponent->BindAxis("MoveRight", this, &AGAM312_FPCharacter::MoveRight); // Bind movement events

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AGAM312_FPCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AGAM312_FPCharacter::LookUpAtRate);

	// DisplayRaycast implementation and Bind DisplayRaycast event (R pressed). 
	PlayerInputComponent->BindAction("Raycast", IE_Pressed, this, &AGAM312_FPCharacter::DisplayRaycast);
	// use keyboard "P" key to Pause the game. (since 'esc' has different function in editor)
	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AGAM312_FPCharacter::PauseGame); // Pause action.
}

USkeletalMeshComponent* AGAM312_FPCharacter::GetMesh1P() const
{
	return Mesh1P;
}


void AGAM312_FPCharacter::OnFire()
{
	if (CurrentMagic >= MagicConsumption)
	{
		// try and fire a projectile
		if (IsValid(ProjectileClass))
		{
			UWorld* const World = GetWorld();
			if (IsValid(World)) // preventing exception throw.
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr)
					                               ? FP_MuzzleLocation->GetComponentLocation()
					                               : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride =
					ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn; // changed to aways spawn.

				// spawn the projectile at the muzzle
				World->SpawnActor<
					AGAM312_FPProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

				UpdatePlayer(0, 10); // consume magic.
				SetActorTickEnabled(false); // stops our regen for a short period before it ticks again.
				FTimerHandle TickHandle;
				GetWorldTimerManager().SetTimer(TickHandle, [this]() { SetActorTickEnabled(true); }, 1.0, false, 2.0);
				// 2 secs interval
			}
		}

		// try and play the sound if specified
		if (IsValid(FireSound))
		{
			UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
		}

		// try and play a firing animation if specified
		if (IsValid(FireAnimation))
		{
			// Get the animation object for the arms mesh
			UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
			if (IsValid(AnimInstance))
			{
				AnimInstance->Montage_Play(FireAnimation, 1.f);
			}
		}
	}
}

inline void AGAM312_FPCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

inline void AGAM312_FPCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

inline void AGAM312_FPCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

inline void AGAM312_FPCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AGAM312_FPCharacter::DisplayRaycast() // RayCasting method.
{
	auto* HitResult{new FHitResult()}; // Hit detection result and properties.
	const auto StartTrace{FirstPersonCameraComponent->GetComponentLocation()};
	// Start position, which is usually set to the component (FPS cam). 
	const auto ForwardVector{FirstPersonCameraComponent->GetForwardVector()};
	// Forward vector of FPS Camera. This is aligned with our crosshair.
	const auto EndTrace{(ForwardVector * LineTraceLength) + StartTrace};
	// End point, which is the forward vector multiplied by a variable that can be set in the editor. 
	const auto* TraceParams{new FCollisionQueryParams()};
	// Other properties included in a line trace result.

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *TraceParams))
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor(255, 0, 0), true); // Draw a debug line.
		GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red,
		                                 FString::Printf(TEXT("You hit: %s"), *HitResult->Component->GetName()));
		// Output name if hit.
	}
}

void AGAM312_FPCharacter::DamageTaken_Implementation(const float& IncomingDamage)
{
	UpdatePlayer(IncomingDamage, 0); // call function to update player attributes (taking damage)
}

void AGAM312_FPCharacter::UpdatePlayer(float health, float magic)
{
	CurrentHealth -= health; // modifying attributes.
	CurrentMagic -= magic; // modifying attributes.
	OnPlayerStatusUpdate.Broadcast(CurrentHealth, CurrentMagic); // broadcasting, passing the player attributes.

	const AGAM312_FPGameMode* GameMode = Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode());
	const bool bGameOver = CurrentHealth <= 0;
	if (IsValid(GameMode))
	{
		GameMode->GameOver(bGameOver); // executing game over event.
	}
}

void AGAM312_FPCharacter::PauseGame()
{
	const AGAM312_FPGameMode* GameMode = Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode());
	if (IsValid(GameMode))
	{
		GameMode->DisplayPauseMenu(); // displaying pause menu.
	}
}
