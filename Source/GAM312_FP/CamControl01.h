// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CamControl01.generated.h"

UCLASS()
class GAM312_FP_API ACamControl01 final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACamControl01();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, Category = "ViewTarget")
	TArray<AActor*> CamArray; // Array of cameras.
	// VARIABLES
	UPROPERTY(EditAnywhere, Category = "ViewTarget")
	float BlendTime{1.0f};
	UPROPERTY(EditAnywhere, Category = "ViewTarget")
	float ChangeInterval{3.0f};
	FTimerHandle CamTimerHandle; // Timer Handle (for Control)
	UPROPERTY(Transient)
	APlayerController* PC; // Our player controller.
	UPROPERTY(Transient)
	APawn* PP;

	int32 CallTracker{0}, CamIndex{0}; // Variables.
};
