// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "GAM312_FPHUD.generated.h"

UCLASS()
class AGAM312_FPHUD : public AHUD
{
	GENERATED_BODY()

public:
	AGAM312_FPHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	UPROPERTY(Transient)
	UTexture2D* CrosshairTex;
};

