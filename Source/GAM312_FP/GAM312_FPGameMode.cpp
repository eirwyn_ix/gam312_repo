// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAM312_FPGameMode.h"
#include "AIChar.h"
#include "AIWaypoint.h"
#include "GAM312_FPCharacter.h"
#include "GAM312_FPHUD.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

#define IfValid(obj) if(IsValid(obj))

AGAM312_FPGameMode::AGAM312_FPGameMode() : Super()
{
	PrimaryActorTick.bCanEverTick = true; // enable ticks.
	PrimaryActorTick.bStartWithTickEnabled = true;
	SetActorTickInterval(1.0); // set interval to 1s.
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(
		TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGAM312_FPHUD::StaticClass();
}

void AGAM312_FPGameMode::BeginPlay()
{
	Super::BeginPlay();

	StartGame(); // start the game with default value.

	//auto CurrentPlayer = SEQPlayer->CreateLevelSequencePlayer(GetWorld(), )
}

void AGAM312_FPGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	UpdateTimer(DeltaSeconds); // called every second.
}

void AGAM312_FPGameMode::DisplayFPHUD()
{
	IfValid(PlayerHUDClass) // assign our widget. check whether it's null.
	{
		IfValid(GetWorld()) // check if world context is a null pointer.
		{
			CurrentWidget = CreateWidget<UUserWidget>(AActor::GetWorld(), PlayerHUDClass);
			IfValid(CurrentWidget) // check if widget is a null pointer.
			{
				CurrentWidget->AddToViewport(); // adding the widget to the view port.
			}
		}
	}
}

void AGAM312_FPGameMode::DisplayPauseMenu() const
{
	const UWorld* World = GetWorld();
	IfValid(World) // Pause Menu Popup.
	{
		APlayerController* PC = Cast<APlayerController>(Player->GetController());
		IfValid(PC)
		{
			PC->bShowMouseCursor = true;
			PC->bEnableClickEvents = true;
			PC->bEnableMouseOverEvents = true;
		}

		UGameplayStatics::SetGamePaused(World, true); // Pause the game.

		UUserWidget* PauseWidget = CreateWidget(GetWorld(), PauseMenuWidgetClass);

		IfValid(PauseWidget)
		{
			PauseWidget->AddToViewport(); // Display Pause Menu.
		}
	}
}

void AGAM312_FPGameMode::StartGame()
{
	// start game with default values.
	const UWorld* World = GetWorld();
	IfValid(World)
	{
		FScopeLock Lock(&WaypointsCriticalSection); // Locking up the Array and Add all the waypoints in the array.
		UGameplayStatics::GetAllActorsOfClass(World, AAIWaypoint::StaticClass(), Waypoints);
		// get all the waypoints, add to array.
		UGameplayStatics::GetAllActorsOfClass(World, AAIChar::StaticClass(), Enemies);
		// get all the enemies, add to array.
		Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(World, 0));
		TimerCounter = 0.0;
		DisplayFPHUD(); // display player HUD.
	}
}

void AGAM312_FPGameMode::GameOver(const bool& bOver) const
{
	if (bOver)
	{
		IfValid(EndGameScreenClass)
		{
			UUserWidget* Widget = CreateWidget(GetWorld(), EndGameScreenClass);
			if (Widget)
			{
				UGameplayStatics::SetGamePaused(GetWorld(), true); // Pause Game.
				CurrentWidget->RemoveFromParent();
				Widget->AddToViewport(); // Displaying End Game Widget.
				const auto Controller = Player->GetController(); // Displaying Mouse Cursor.
				if (IsValid(Controller))
				{
					APlayerController* PC = Cast<APlayerController>(Controller);
					PC->bShowMouseCursor = true;
					PC->bEnableClickEvents = true;
					PC->bEnableMouseOverEvents = true;
				}
			}
		}
	}
}

void AGAM312_FPGameMode::UpdateTimer(const float& dT) // update game time.
{
	TimerCounter += dT; // count towards timer.

	OnTimerUpdate.Broadcast(TimerCounter); // broadcasting timer counter.
}

void AGAM312_FPGameMode::AddScore(const uint8_t& Score)
{
	TotalScore += Score; // add score to the total.
	TotalKill++; // increment kill counter.

	OnScoreUpdate.Broadcast(TotalScore, TotalKill); // broadcast with variables.

	GameOver(TotalKill >= Enemies.Num());
}
