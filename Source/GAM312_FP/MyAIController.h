// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>
#include "CoreMinimal.h"
#include "AIController.h"
#include "MyAIController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_FP_API AMyAIController : public AAIController
{
	GENERATED_BODY()

	typedef float f;
	typedef bool b;
	typedef int i;

public:
	AMyAIController();

	virtual void Tick(float DeltaSeconds) override;

	virtual void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

protected:
	virtual void BeginPlay() override;

private:

	virtual void UpdateControlRotation(float DeltaTime, bool bUpdatePawn) override;
	virtual FRotator GetControlRotation() const override;

	// VARIABLES
	f AILoseSightRadius{500.f}, AISightLife{5.f}, AISightRadius{1500.f}, AIFieldOfView{75.f}, DistanceBetween{0.0f};
	b bIsPlayerDetected{false};
	i index{0};
	FTimerHandle TimerHandle;
	FCriticalSection TargetsCriticalSection;

	//UPROPERTY(Transient)
	//TArray<AActor*> Targets {nullptr, nullptr};

	std::vector<AActor*> mTargets {nullptr, nullptr};

	UPROPERTY(Transient)
	class AGAM312_FPCharacter* Player;
	UPROPERTY(Transient)
	class AAIChar* MyAICharacter;

	UFUNCTION() // bind to event (had to be a UFUNCTION).
	void OnObjectDetected(const TArray<AActor*>& DetectedObjects);

	UFUNCTION()
	void StopController(FVector Location);

};
