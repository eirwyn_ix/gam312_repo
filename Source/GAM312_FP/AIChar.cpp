// Fill out your copyright notice in the Description page of Project Settings.


#include "AIChar.h"
#include "GAM312_FPGameMode.h"
#include "MyAIController.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"

#define debug(m) GEngine->AddOnScreenDebugMessage(1, 100, FColor::Green, m);

// Sets default values
AAIChar::AAIChar()
{
	PrimaryActorTick.bCanEverTick = false; // No tick required. Saving Performance.
	PrimaryActorTick.bStartWithTickEnabled = false;

	// DEFAULTS
	GetCharacterMovement()->bOrientRotationToMovement = true; // adjusting orientation.
	GetCharacterMovement()->RotationRate = FRotator(.0, 250.0, .0);

	constexpr float SphereRadius{20};
	// HIT DETECTION SPHERE
	HitSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Hit Volume"));
	HitSphere->InitSphereRadius(SphereRadius);
	HitSphere->SetCollisionProfileName("OverlapAll", true);
	HitSphere->SetGenerateOverlapEvents(true);
	HitSphere->OnComponentBeginOverlap.AddDynamic(this, &AAIChar::OnOverlap);
	HitSphere->OnComponentEndOverlap.AddDynamic(this, &AAIChar::EndOverlap);
	HitSphere->SetupAttachment(RootComponent); // attach to the root component.
	HitSphere->SetRelativeLocation(FVector(120.f, 0.f, 0.f)); // offset the z position by SphereRadius/2

	//Setting AI Controller and Possession.
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = AMyAIController::StaticClass();
}

// Called when the game starts or when spawned
void AAIChar::BeginPlay()
{
	Super::BeginPlay();

	const UWorld* World = GetWorld();
	if (IsValid(World))
	{
		MyGameMode = Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode());
		// debug.
		if (IsValid(MyGameMode))
		{
			debug(MyGameMode->GetName());
		}
			debug(MyGameMode->GetName());

		GetWorldTimerManager().SetTimer(TimerHandle, this, &AAIChar::ChangeIndex, 10, false);
	}
}

// Called every frame
void AAIChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AAIChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AAIChar::DamageTaken_Implementation(const float& IncomingDamage)
{
	IDamageTaken_Interface::DamageTaken_Implementation(IncomingDamage);

	HealthPoint -= IncomingDamage; // dealing damage
	if (IsValid(HitSound))
	{
		UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation()); // play damage sound.
	}
	if (HealthPoint <= 0) // validate status.
	{
		if (!bDied)
		{
			bDied = true;
			HitSphere->DestroyComponent(bDied);
			OnAiDeath.Broadcast(GetActorLocation());
			MyGameMode->AddScore(25);
		}
	}
}

void AAIChar::ChangeIndex()
{
	const auto& NextWaypoint = MyGameMode->Waypoints[FMath::RandRange(0, MyGameMode->Waypoints.Num() - 1)];
	if (IsValid(NextWaypoint))
	{
		CurrentWapoint = NextWaypoint;
		GetWorldTimerManager().ClearTimer(TimerHandle);
	}
}

void AAIChar::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                        int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bInRadius = OtherActor != this && OtherActor->GetClass() != GetClass() && OtherActor->GetClass()->
		ImplementsInterface(UDamageTaken_Interface::StaticClass());
	// in radius flag changed to true in order to enter a attack state (in animation instance)
	Attacking(bInRadius);
	GetWorldTimerManager().SetTimer(AttackHandle, [this]() { Attacking(bInRadius); }, 2.f, true);
}

void AAIChar::EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
                         int32 OtherBodyIndex)
{
	// making sure the actor is not itself or of the same class and has interface implemented.
	if(OtherActor != this && OtherActor->GetClass() != GetClass() && GetClass()->
		ImplementsInterface(UDamageTaken_Interface::StaticClass()))
	{
		bInRadius = false;
		GetWorldTimerManager().ClearTimer(AttackHandle);
	}
}

void AAIChar::DealDamage() const
{
	TArray<AActor*> HitActors;
	HitSphere->GetOverlappingActors(HitActors);
	for (const auto& actor : HitActors)
	{
		const auto& ActorClass = actor->GetClass();

		if (actor != this && ActorClass != GetClass() && ActorClass->ImplementsInterface(
			UDamageTaken_Interface::StaticClass()))
		{
			// making sure the actor is not itself or of the same class and has interface implemented.
			Execute_DamageTaken(actor, 5);
			if (IsValid(AttackSound))
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), AttackSound, GetActorLocation());
			}
		}
	}
}

void AAIChar::Attacking(const bool& IsInRadius) const
{
	if (IsInRadius && IsValid(AttackAnimation))
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (IsValid(AnimInstance))
		{
			AnimInstance->Montage_Play(AttackAnimation, 1.f);
		}
	}
}
