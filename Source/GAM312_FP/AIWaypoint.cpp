// Fill out your copyright notice in the Description page of Project Settings.


#include "AIWaypoint.h"
#include "AIChar.h"
#include "GAM312_FPGameMode.h"
#include "MyAIController.h"
#include "Components/BoxComponent.h"
#include "LevelSequenceActor.h"

#define AITo(Destination) MyAICharacter->mController->MoveTo(Destination)
#define Debug_Int(Cd, i) GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Cd, FString::Printf(TEXT("%i"), i));
#define Debug_Str(Cd, s) GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Cd, FString::Printf(TEXT(s)));


// Sets default values
AAIWaypoint::AAIWaypoint()
{
	PrimaryActorTick.bCanEverTick = false; // disable tick to safe performance.
	PrimaryActorTick.bStartWithTickEnabled = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	// set up scene root. (for the ease of locating)
	Root->SetMobility(EComponentMobility::Static); // mobility set to static.
	SetRootComponent(Root);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box")); // set up overlapping box.
	BoxComponent->SetMobility(EComponentMobility::Static);
	BoxComponent->SetupAttachment(GetRootComponent()); // attach to root.
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AAIWaypoint::OnPawnEnter); // event binding 
	BoxComponent->SetBoxExtent(FVector(200, 200, 200));
	// setting the box extend to about the size of the character pawn.

	for (size_t x{0}; x < 300; x++)
	{
		CamPos.Add(FVector(x * x, x / 1, x * 2));
	}
}

// Called when the game starts or when spawned
void AAIWaypoint::BeginPlay()
{
	Super::BeginPlay();

	ULevelSequencePlayer::CreateLevelSequencePlayer(GetWorld(), SEQ, FMovieSceneSequencePlaybackSettings(), OutActor);
	if (IsValid(OutActor))
	{
		OutActor->GetSequencePlayer()->Play();
		Debug_Str(Green, "Level Sequence Played");
	}
	Debug_Int(Green, CamPos.Num());
}

void AAIWaypoint::OnPawnEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor,
                              UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                              const FHitResult& SweepResult)
{
	// When the AI reaches waypoint, change its destination.
	AAIChar* MyAICharacter = Cast<AAIChar>(OtherActor);
	const AGAM312_FPGameMode* MyGameMode = Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode());

	if (IsValid(MyAICharacter) && IsValid(MyGameMode))
	{
		const auto NextWaypoint = MyGameMode->Waypoints[FMath::RandRange(0, MyGameMode->Waypoints.Num() - 1)];
		if (IsValid(NextWaypoint))
		{
			MyAICharacter->CurrentWapoint = NextWaypoint;
			AITo(NextWaypoint);
		}
	}
}
