// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

/**
 * 
 */
class UButton;

UCLASS()
class GAM312_FP_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* ContinueBtn;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* QuitBtn;
	UPROPERTY(Transient)
	class AGAM312_FPCharacter* Player;
	UPROPERTY(Transient)
	APlayerController* PlayerController;

	virtual void NativeConstruct() override;
	UFUNCTION()
	void OnContinueClick();
	UFUNCTION()
	void OnQuitClick();

};
