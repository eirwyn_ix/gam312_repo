// Fill out your copyright notice in the Description page of Project Settings.


#include "StartMenuWidget.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UStartMenuWidget::NativeConstruct()
{
	if (IsValid(StartBtn))
	{
		StartBtn->OnClicked.AddDynamic(this, &UStartMenuWidget::OnStartClick);
	}
	if (IsValid(QuitBtn))
	{
		QuitBtn->OnClicked.AddDynamic(this, &UStartMenuWidget::OnQuitClick);
	}
}

void UStartMenuWidget::OnStartClick()
{
	const UWorld* World = GetWorld();
	if (IsValid(World))
	{
		UGameplayStatics::OpenLevel(World, TEXT("GAM312_FP_Persistent"));
	}
}

void UStartMenuWidget::OnQuitClick()
{
	const UWorld* World = GetWorld();
	if (IsValid(World))
	{
		UKismetSystemLibrary::ExecuteConsoleCommand(World, TEXT("quit"));
	}
}
