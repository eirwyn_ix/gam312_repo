// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAIController.h"
#include <vector>
#include "AIChar.h"
#include "GAM312_FPCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Sight.h"

#define PerceptComp AAIController::GetPerceptionComponent()
#define DefaultComp(comp, text) CreateDefaultSubobject<comp>(TEXT(text))
#define EnableSight(bVar) SightConfig->DetectionByAffiliation.bVar=true
#define IfValid(obj) if(IsValid(obj))
#define StopTimer(handle) GetWorldTimerManager().ClearTimer(handle)

AMyAIController::AMyAIController()
{
	PrimaryActorTick.bCanEverTick = true; // enable tick.
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = 0.2f;

	UAISenseConfig_Sight* SightConfig = DefaultComp(UAISenseConfig_Sight, "Sight Config");
	SetPerceptionComponent(*DefaultComp(UAIPerceptionComponent, "Perception Component"));

	// DEFAULTS
	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightLife);

	EnableSight(bDetectEnemies);
	EnableSight(bDetectFriendlies);
	EnableSight(bDetectNeutrals);

	PerceptComp->SetDominantSense(*SightConfig->GetSenseImplementation());
	// setting the sense.
	PerceptComp->OnPerceptionUpdated.AddDynamic(this, &AMyAIController::OnObjectDetected);
	// binding.
	PerceptComp->ConfigureSense(*SightConfig); // sight config.
}

void AMyAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	mTargets[1] = MyAICharacter->CurrentWapoint;
	if (GetPawn()->GetDistanceTo(Player) > AISightRadius)
	{
		bIsPlayerDetected = false;
		MyAICharacter->GetCharacterMovement()->MaxWalkSpeed = !MyAICharacter->bInRadius * (1 + bIsPlayerDetected) * 150;
		// change speed.
		MoveToActor(mTargets[!bIsPlayerDetected], 5.0f);
		SetActorTickEnabled(bIsPlayerDetected);
	}
}

void AMyAIController::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0)); // our player.

	IfValid(Player)
	{
		FScopeLock Lock(&TargetsCriticalSection); // set target array.
		mTargets = {Player, Player};
	}

	GetWorldTimerManager().SetTimer(TimerHandle, [this]()
	{
		MyAICharacter = Cast<AAIChar>(GetPawn());
		IfValid(MyAICharacter)
		{
			MyAICharacter->mController = this;
			mTargets[1] = MyAICharacter->CurrentWapoint;
			MoveToActor(mTargets[!bIsPlayerDetected], 5.0f);
			MyAICharacter->OnAiDeath.AddDynamic(this, &AMyAIController::StopController);
			StopTimer(TimerHandle);
		}
	}, 1.f, true);
}

void AMyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Green, // for debugging purpose.
	                                 FString::Printf(TEXT("Movement Complete")));
}

void AMyAIController::UpdateControlRotation(float DeltaTime, bool bUpdatePawn)
{
	Super::UpdateControlRotation(DeltaTime, bUpdatePawn);
}

FRotator AMyAIController::GetControlRotation() const
{
	IfValid(GetPawn())
	{
		return FRotator(0, GetPawn()->GetActorRotation().Yaw, 0);
	}
	return FRotator(0, 0, 0);
}

void AMyAIController::OnObjectDetected(const TArray<AActor*>& DetectedObjects)
{
	IfValid(MyAICharacter)
	{
		bIsPlayerDetected = true; // flip detection boolean value to true.
		MyAICharacter->GetCharacterMovement()->MaxWalkSpeed = !MyAICharacter->bInRadius * (1 + bIsPlayerDetected) * 150;
		// change speed.
		MoveToActor(mTargets[!bIsPlayerDetected], 5.0f);
		SetActorTickEnabled(bIsPlayerDetected);
		for (size_t i{0}; i < DetectedObjects.Num(); i++) // for debugging.
		{
			if (Player == DetectedObjects[i])
			{
				GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Green,
				                                 DetectedObjects[i]->GetName());
			}
		}
	}
}

void AMyAIController::StopController(FVector Location = {0, 0, 0})
{
	GetPerceptionComponent()->DestroyComponent();
	StopMovement();
	Destroy();
}
