// Fill out your copyright notice in the Description page of Project Settings.


#include "OverlappingTrigger.h"

#include "Components/SphereComponent.h"
#include "Components/SpotLightComponent.h"

// Sets default values
AOverlappingTrigger::AOverlappingTrigger()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = 0.2f;

	//StaticMeshAsset
	StaticMeshAsset = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Asset")); // static mesh
	StaticMeshAsset->SetMobility(EComponentMobility::Static);
	StaticMeshAsset->SetCollisionProfileName("NoCollision", false); // default static mesh component.
	StaticMeshAsset->SetGenerateOverlapEvents(false);
	StaticMeshAsset->SetRelativeLocation(FVector(0.0, 0.0, 20.0));
	StaticMeshAsset->SetWorldScale3D(FVector(2.0, 2.0, 2.0));
	// turn off the generate overlap event (instead use trigger to generate event)
	RootComponent = StaticMeshAsset; // set up to be the root component.

	// setting the default static mesh.
	/*
	 * 	static ConstructorHelpers::FObjectFinder<UStaticMesh> FountainAsset(
		TEXT("/Game/FX/Fountain/fountain_1st_mesh.fountain_1st_mesh"));

	if (FountainAsset.Succeeded())
	{
		StaticMeshAsset->SetStaticMesh(FountainAsset.Object);
	}

	// setting the animation curve
	static ConstructorHelpers::FObjectFinder<UCurveFloat> FountainCurve(TEXT("/Game/FX/skull/skull_curve.skull_curve"));
	if (FountainCurve.Succeeded())
	{
		FXCurveFloat = FountainCurve.Object; // assign the curve.
	}
	 */


	//SpotLight
	SpotLight = CreateDefaultSubobject<USpotLightComponent>(TEXT("Light Source"));
	SpotLight->SetMobility(EComponentMobility::Static);
	SpotLight->Intensity = LightIntensity;
	SpotLight->SetAttenuationRadius(10000);
	SpotLight->SetInnerConeAngle(30);
	SpotLight->SetOuterConeAngle(45);
	SpotLight->SetVisibility(false);
	SpotLight->SetRelativeLocation(FVector(0.f, 0.f, 750.f)); // set the relative location for the light.
	SpotLight->SetRelativeRotation(FRotator(-90.f, 0.f, 0.f)); // 90 degree clock wise rotation. (point downwards)
	SpotLight->SetupAttachment(RootComponent); // attach to the root component.
	SpotLight->SetUseTemperature(true);
	SpotLight->SetTemperature(3200);

	//TriggerSphere
	TriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Trigger"));
	TriggerSphere->SetMobility(EComponentMobility::Static);
	TriggerSphere->SetCollisionProfileName("Trigger");
	TriggerSphere->InitSphereRadius(TriggerRadius);
	TriggerSphere->SetupAttachment(RootComponent); // attach to the root component.

	// overlapping event.
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &AOverlappingTrigger::OnOverlapBegin);
	TriggerSphere->OnComponentEndOverlap.AddDynamic(this, &AOverlappingTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AOverlappingTrigger::BeginPlay()
{
	Super::BeginPlay();

	// DYNAMIC MATERIAL SETUP
	Material = StaticMeshAsset->GetMaterial(0); // prep for the dynamic material instance.
	DynamicMaterial = UMaterialInstanceDynamic::Create(Material, this);
	// making a new dynamic material instance from the material above.
	StaticMeshAsset->SetMaterial(0, DynamicMaterial); // set the first material slot to the dynamic material instance.

	FOnTimelineFloat TimelineProgress; // timeline will update the FXUpdate event.
	TimelineProgress.BindUFunction(this, FName("FXUpdate"));
	FOnTimelineEvent TimelineFinished; // bind FXEnd and  TimelineFinished together.
	TimelineFinished.BindUFunction(this, FName("FXEnd"));

	FXTimeline.AddInterpFloat(FXCurveFloat, TimelineProgress); // add a float interpolation to the float timeline.
	FXTimeline.SetTimelineFinishedFunc(TimelineFinished); // set delegate to call when the timeline is finished
}

// Called every frame
void AOverlappingTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FXTimeline.TickTimeline(DeltaTime); // time line ticks
}

void AOverlappingTrigger::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                         UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                         const FHitResult& SweepResult)
{
	SetActorTickEnabled(true);
	SpotLight->SetVisibility(OtherActor && OtherActor != this && OtherComp); // set visibility when the player steps in.
	if (IsValid(FXCurveFloat))
	{
		FXTimeline.Play(); // play timeline.
	}
}

void AOverlappingTrigger::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
                                       UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	SpotLight->SetVisibility(!(OtherActor && OtherActor != this && OtherComp));
	// set visibility when the player steps out.
	if (IsValid(FXCurveFloat))
	{
		FXTimeline.Reverse(); // event #2: reverse timeline.
	}
}

void AOverlappingTrigger::FXUpdate(float F)
{
}

void AOverlappingTrigger::FXEnd()
{
	SetActorTickEnabled(false);
	GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Green,
	                                 FString::Printf(TEXT("Animation Ended")));
}
