// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include <random>
#include "Components/BillboardComponent.h"

// Sets default values
ASpawner::ASpawner()
{
	PrimaryActorTick.bCanEverTick = false; // Disable Ticks to save performance.
	PrimaryActorTick.bStartWithTickEnabled = false;

	Marker = CreateDefaultSubobject<UBillboardComponent>(TEXT("Spawner Marker"));
	Marker->SetMobility(EComponentMobility::Static);
	RootComponent = Marker;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	srand(time(nullptr)); // seed
	// Prepare random float. need to include <random> Library.
	// look at the variables in the editor and spawn actors.
	for (size_t i{0}; i < SpawnNumbers.Num(); i++)
	{
		if (SpawnNumbers[i] && SpawnActors[i])
		{
			ArraySpawn(SpawnNumbers[i], SpawnActors[i], GetActorLocation());
		}
	}
	Destroy(); // Destroy actor.
}

void ASpawner::ArraySpawn(const int& SpawnNumber, const TSubclassOf<AActor>& SpawnActor, const FVector& ActorLocation) const
{
	std::random_device rd;
	std::default_random_engine rEng(rd());
	const auto Halfen{[](const float& x) { return x * 100 / 2; }}; // lambda expression redefine bound.
	//	uniform distribution for x,y,z.
	const URDistr distrX(-Halfen(SpawnBoxExtendInMeters.X),
	                     Halfen(SpawnBoxExtendInMeters.X));
	const URDistr distrY(-Halfen(SpawnBoxExtendInMeters.Y),
	                     Halfen(SpawnBoxExtendInMeters.Y));
	const URDistr distrZ(-Halfen(SpawnBoxExtendInMeters.Z),
	                     Halfen(SpawnBoxExtendInMeters.Z)); 
	// pick every actor in the array and spawn it at the location according to the position stored in the array.
	// rotate randomly according to Y axis. (it appears that Y is up in rotation)
	for (size_t i{0}; i < SpawnNumber; i++)
	{
		//use random float for position.
		auto SpawnPosition = FVector(distrX(rEng) + ActorLocation.X, distrY(rEng) + ActorLocation.Y,
		                             distrZ(rEng) + ActorLocation.Z);
		GetWorld()->SpawnActor<AActor>(SpawnActor, SpawnPosition, FRotator(.0, rand() % 360, .0));
		// random rotation.
	}
}
