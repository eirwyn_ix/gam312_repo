// Fill out your copyright notice in the Description page of Project Settings.


#include "AISpawner.h"
#include "AIChar.h"
#include "AIWaypoint.h"

AAISpawner::AAISpawner()
{
	SpawnNumbers.Add(10);
	SpawnActors.Add(AAIChar::StaticClass());

	SpawnNumbers.Add(25);
	SpawnActors.Add(AAIWaypoint::StaticClass());
}
