// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "AI_AnimInstance.generated.h"

/**
 * 
 */
UCLASS(Transient, Blueprintable, HideCategories=AnimInstance, BlueprintType)
class GAM312_FP_API UAI_AnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UAI_AnimInstance();

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;


	UPROPERTY(BlueprintReadWrite, Category = "Generic")
	float speed{0};
	UPROPERTY(BlueprintReadWrite, Category = "Generic")
	bool bDied{false};
	UPROPERTY(BlueprintReadWrite, Category = "Generic")
	bool bInAttackRadius{false};
	UPROPERTY(Transient)
	class AAIChar* MyAICharacter;
};
