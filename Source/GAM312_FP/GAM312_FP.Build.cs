// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GAM312_FP : ModuleRules
{
	public GAM312_FP(ReadOnlyTargetRules Target) : base(Target)
	{
		
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
		PublicDependencyModuleNames.AddRange(new string[] { "UMG", "Slate", "SlateCore", "CinematicCamera" });
		PublicDependencyModuleNames.AddRange(new string[] { "MovieScene", "LevelSequence", "MovieSceneTools" });
	}
}
