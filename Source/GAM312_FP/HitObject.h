// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Actor.h"
#include "HitObject.generated.h"

UCLASS()
class GAM312_FP_API AHitObject final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHitObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	FTimeline FXTimeline; // Timeline
	UPROPERTY(EditAnywhere, Category = "Timeline", Transient)
	UCurveFloat* FXCurveFloat;

	UPROPERTY(Transient) // Variables for storing materials later.
		UMaterialInterface* Material{nullptr};
	UPROPERTY(Transient)
		UMaterialInstanceDynamic* DynamicMaterial {nullptr};
	UPROPERTY(VisibleAnywhere, Transient) // declare a static mesh component.
	UStaticMeshComponent* StaticMeshAsset;
	UPROPERTY(VisibleAnywhere, Category = "Trigger", Transient) // declare a spherical trigger.
	class USphereComponent* HitSphere;
	UPROPERTY(Transient)
	USoundBase* SoundWave;

	
	UFUNCTION() // declare a hit event.
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION() // declare FX event.
	void FXUpdate(float F);
	UFUNCTION()
	void FXEnd();
};
