// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "DamageTaken_Interface.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UDamageTaken_Interface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GAM312_FP_API IDamageTaken_Interface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, Category = "Interact")
	void DamageTaken(const float& IncomingDamage); // interface implementation.
};
