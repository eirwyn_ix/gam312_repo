// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Spawner.h"
#include "AISpawner.generated.h"

/**
 * 
 */
UCLASS()
class GAM312_FP_API AAISpawner final : public ASpawner
{
	GENERATED_BODY()

	public:
	// Sets default values for this actor's properties
	AAISpawner();
};
