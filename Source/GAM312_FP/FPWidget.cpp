// Fill out your copyright notice in the Description page of Project Settings.


#include "FPWidget.h"
#include "GAM312_FPCharacter.h"
#include "GAM312_FPGameMode.h"
#include "Kismet/GameplayStatics.h"

void UFPWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (IsValid(Player))
	{
		Player->OnPlayerStatusUpdate.AddDynamic(this, &UFPWidget::OnPlayerUpdate);
		// bind the player status update delegate;
	}
	if (IsValid(GetWorld())) // check if world context is valid.
	{
		GameMode = Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode());
		if (IsValid(GameMode)) // checking if game mode is valid.
		{
			GameMode->OnScoreUpdate.AddDynamic(this, &UFPWidget::OnScoreUpdate); // bind the score update delegate.
			GameMode->OnTimerUpdate.AddDynamic(this, &UFPWidget::OnTimeUpdate); // bind the timer update.
		}
	}
	HealthBar = 1; // defalut display value.
	MagicBar = 1;
}

inline void UFPWidget::OnPlayerUpdate(float health, float magic)
{
	HealthBar = health / 100;
	MagicBar = magic / 100;
}

void UFPWidget::OnScoreUpdate(int32 Score, int32 NumberOfKill)
{
	Scores = Score;
	KillCount = NumberOfKill;
	ProgressionBar = static_cast<float>(KillCount) / GameMode->Enemies.Num();
}

void UFPWidget::OnTimeUpdate(float TimeInSecs)
{
	TimerMins = static_cast<int>(TimeInSecs / 60.0);
	TimerSecs = static_cast<int>(FMath::Fmod(TimeInSecs, 60));
}


