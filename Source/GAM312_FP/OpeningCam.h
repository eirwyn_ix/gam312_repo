// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OpeningCam.generated.h"

UCLASS()
class GAM312_FP_API AOpeningCam final : public AActor
{
	GENERATED_BODY()

	typedef float f;
	
public:	
	// Sets default values for this actor's properties
	AOpeningCam();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Transient) // Scene Root
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, Transient) // camera actor.
	class UCineCameraComponent* CineCam;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	f t {0}, x {0}, y {0}, z{0};

	UPROPERTY(EditAnywhere, Category="Camera", Transient) // actor reference.
	TArray<AActor*> Targets;
	UPROPERTY(EditAnywhere, Category="Camera") // rotation distance and speed
	float dist{2500};
	UPROPERTY(EditAnywhere, Category="Camera") // stretch curve.
	float speed{-1};
	UPROPERTY(EditAnywhere, Category="Camera") // stretch curve.
	float duration{10};
	UPROPERTY(EditAnywhere, Category="Camera") // stretch curve.
	FVector Offset{0, 0, -150};

	void UpdateLocation(const float& F, const FVector& dir);
};


