// Fill out your copyright notice in the Description page of Project Settings.


#include "CamControl01.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACamControl01::ACamControl01()
{
	PrimaryActorTick.bStartWithTickEnabled = false; // disable ticks to save performance.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void ACamControl01::BeginPlay()
{
	Super::BeginPlay();

	CallTracker = CamArray.Num();
	PC = UGameplayStatics::GetPlayerController(this, 0);
	PP = UGameplayStatics::GetPlayerPawn(this, 0);

	auto TimerEvent{
		// handles camera selection.
		[this]()
		{
			// Check if the value of CallTracker is greater than 0, clear the camera timer handle all have been visited.
			0 < CallTracker--
				? GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::White, TEXT("Move to the next"))
				: GetWorldTimerManager().ClearTimer(CamTimerHandle);
			// Check if camera timer handle is valid, set View Target to either the next camera in the array or to the player pawn.
			CamTimerHandle.IsValid()
				? PC->SetViewTargetWithBlend(CamArray[CamIndex++], BlendTime)
				: PC->SetViewTargetWithBlend(PP, BlendTime);
		}
	};
	GetWorldTimerManager().SetTimer(CamTimerHandle, TimerEvent, ChangeInterval, true, 1.0);
}
