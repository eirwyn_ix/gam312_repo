// Fill out your copyright notice in the Description page of Project Settings.


#include "OpeningCam.h"
#include "AIChar.h"
#include "CineCameraComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AOpeningCam::AOpeningCam()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component")); // set up scene root. (for the ease of locating)
	Root->SetMobility(EComponentMobility::Movable); // mobility set to static.
	SetRootComponent(Root);

	// DEFAULTS
	CineCam = CreateDefaultSubobject<UCineCameraComponent>(TEXT("Break Object"));
	CineCam->SetFieldOfView(60);
	CineCam->SetupAttachment(Root);

}

// Called when the game starts or when spawned
void AOpeningCam::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIChar::StaticClass(), Targets);
	while (!IsValid(Targets[0]))
	{
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIChar::StaticClass(), Targets); // put AI characters in an array
	}

	SetActorLocation(Targets[0]->GetActorLocation()); // save the offset location.
	SetActorTickEnabled(true);
}

// Called every frame
void AOpeningCam::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	t += DeltaTime;
	UpdateLocation(t, (Targets[0]->GetActorLocation() - CineCam->GetComponentLocation()));
}

inline void AOpeningCam::UpdateLocation(const float& F, const FVector& dir)
{
	const float T = speed * F;
	const float g = 0.5 * exp(0.15 * T);

	x = g * cos(2 * T);
	y = g * sin(2 * T);
	z = g;

	CineCam->SetRelativeLocationAndRotation(dist * FVector(x, y, z), dir.GetSafeNormal().ToOrientationRotator());
	SetActorTickEnabled(F < duration);
}

