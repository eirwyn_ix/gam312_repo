// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "StartMenuWidget.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class GAM312_FP_API UStartMenuWidget : public UUserWidget
{
	GENERATED_BODY()

	protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* StartBtn;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* QuitBtn;
	UPROPERTY(Transient)

	class AGAM312_FPCharacter* Player;
	UPROPERTY(Transient)
	APlayerController* PlayerController;

	virtual void NativeConstruct() override;
	UFUNCTION()
	void OnStartClick();
	UFUNCTION()
	void OnQuitClick();
	
};
