// Fill out your copyright notice in the Description page of Project Settings.


#include "PauseMenuWidget.h"
#include "GAM312_FPCharacter.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UPauseMenuWidget::NativeConstruct()
{
	if (IsValid(ContinueBtn))
	{
		ContinueBtn->OnClicked.AddDynamic(this, &UPauseMenuWidget::OnContinueClick);
	}
	if (IsValid(QuitBtn))
	{
		QuitBtn->OnClicked.AddDynamic(this, &UPauseMenuWidget::OnQuitClick);
	}
}

void UPauseMenuWidget::OnContinueClick()
{
	if (IsValid(GetWorld()))
	{
		UGameplayStatics::SetGamePaused(GetWorld(), false);
		Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
		const auto& Controller = Player->GetController(); // getting the player controller.
		if (IsValid(Controller))
		{
			APlayerController* PC = Cast<APlayerController>(Controller); // give the control back.
			PC->bShowMouseCursor = false;
			PC->bEnableClickEvents = false;
			PC->bEnableMouseOverEvents = false;
		}
		RemoveFromViewport(); // remove the widget from viewport.
	}
}

void UPauseMenuWidget::OnQuitClick()
{
	UWorld* World = GetWorld();
	Player = Cast<AGAM312_FPCharacter>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (IsValid(World))
	{
		PlayerController = Cast<APlayerController>(Player->GetController());
		PlayerController->ConsoleCommand("quit");
	}
}
