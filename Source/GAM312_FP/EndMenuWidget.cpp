// Fill out your copyright notice in the Description page of Project Settings.


#include "EndMenuWidget.h"
#include "GAM312_FPGameMode.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

#define IfValid(obj) if(IsValid(obj))

void UEndMenuWidget::NativeConstruct()
{ // Bindings.
	IfValid(MainMenuBtn)
	{
		MainMenuBtn->OnClicked.AddDynamic(this, &UEndMenuWidget::OnMainMenuClick); // main menu button binding.
	}
	IfValid(QuitBtn)
	{
		QuitBtn->OnClicked.AddDynamic(this, &UEndMenuWidget::OnQuitClick); // quit button binding.
	}
}

void UEndMenuWidget::NativeOnInitialized() // using the initialize override since the widget only gets called once.
{
	Super::NativeOnInitialized();

	const AGAM312_FPGameMode* GameMode {Cast<AGAM312_FPGameMode>(GetWorld()->GetAuthGameMode())};
	IfValid(GameMode)
	{ // displaying score and game time (mins and secs) retrieve values from the game mode.
		Score = GameMode->TotalScore;
		const auto GameTime = GameMode->TimerCounter;
		TimeMins = static_cast<int>(GameTime / 60.0);
		TimeSecs = static_cast<int>(FMath::Fmod(GameTime, 60));
	}
}

void UEndMenuWidget::OnMainMenuClick()
{
	const UWorld* World = GetWorld();
	IfValid(World) // main menu button function. open the main menu level.
	{
		UGameplayStatics::OpenLevel(World, TEXT("MainMenu"));
	}
}

void UEndMenuWidget::OnQuitClick()
{ // quit function.
	const UWorld* World = GetWorld();
	IfValid(World)
	{
		UKismetSystemLibrary::ExecuteConsoleCommand(World, TEXT("quit"));
	}
}
