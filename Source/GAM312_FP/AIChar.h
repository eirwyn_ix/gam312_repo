// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Interfaces/DamageTaken_Interface.h"
#include "AIChar.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAiDeathSigniture, FVector, Location);

UCLASS()
class GAM312_FP_API AAIChar : public ACharacter, public IDamageTaken_Interface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAIChar();

	UPROPERTY(BlueprintAssignable)
	FOnAiDeathSigniture OnAiDeath;

	bool bInRadius{false};

protected:

virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "HitVolume", Transient) // declare a spherical trigger.
	class USphereComponent* HitSphere;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay, Transient)
	UAnimMontage* AttackAnimation;

	UPROPERTY(EditAnywhere, Transient)
	USoundBase* HitSound;

	UPROPERTY(EditAnywhere, Transient)
	USoundBase* AttackSound;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool bDied{false};

	void DealDamage() const;

	UPROPERTY(Transient)
	AActor* CurrentWapoint;

	UPROPERTY(Transient)
	class AMyAIController* mController;

	void ChangeIndex();

private:

	//UPROPERTY()
	FCriticalSection WaypointsCriticalSection;

	UPROPERTY(Transient)
	class AGAM312_FPGameMode* MyGameMode; // caching our game mode.

	FTimerHandle TimerHandle, AttackHandle; // two timer handle. (detection interval and attack interval)

	float HealthPoint{100};

	bool bSpeedUp{false};

	virtual void DamageTaken_Implementation(const float& IncomingDamage) override; // interface implementation.

	UPROPERTY(Transient)
	TArray<AActor*> HitTargets;

	UFUNCTION() // one that does hit detection when AI attacks. (bind with delegate)
	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION() // out of damage radius of the enemy. (bind with delegate)
	void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void Attacking(const bool& IsInRadius) const;
};