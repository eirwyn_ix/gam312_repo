// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAM312_FPGameMode.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTimerUpdateSigniture, float, TimeinSecs); // delegate.
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FScoreUpdateSigniture, int32, Score, int32, NumberOfKill); // delegate.

class UUserWidget;

UCLASS()
class AGAM312_FPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	AGAM312_FPGameMode();

	int TotalScore{0}, TotalKill{0}; // Tracking Score and Kill count.

	float TimerCounter{0.0}; // Tracking game time.

	UPROPERTY(BlueprintAssignable) // Signatures
	FScoreUpdateSigniture OnScoreUpdate;

	UPROPERTY(BlueprintAssignable) // Signatures
	FTimerUpdateSigniture OnTimerUpdate;
	
	UPROPERTY(Transient) // array for waypoints.
	TArray<AActor*> Waypoints;

	UPROPERTY(Transient) // array for ai enemies.
	TArray<AActor*> Enemies;

	void AddScore(const uint8_t& Score); // Add Score when enemy is killed.

	virtual void Tick(float DeltaSeconds) override;

	void DisplayFPHUD() ; // display player hud.

	void DisplayPauseMenu() const; // display pause menu.

	void StartGame()  ; // start game, propagate arrays.

	UFUNCTION(BlueprintCallable) // ending game.
	void GameOver(const bool& bOver) const;

protected:
	virtual void BeginPlay() override;

	// For creating widget. Editable in editor.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Generic", Meta = (BlueprintProtected = "true"))
	TSubclassOf<UUserWidget> PlayerHUDClass; 

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Generic", Meta = (BlueprintProtected = "true" ))
	TSubclassOf<UUserWidget> PauseMenuWidgetClass; 

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Generic", Meta = (BlueprintProtected = "true"))
	TSubclassOf<UUserWidget> EndGameScreenClass;

private:

	UPROPERTY(Transient)
	class AGAM312_FPCharacter* Player; // caching player.

	FCriticalSection WaypointsCriticalSection; // Waypoints array.

	// HUD Components.
	UPROPERTY(Transient)
	UUserWidget* CurrentWidget; // Player Hud.

	void UpdateTimer(const float& dT); // tracking time.
};

