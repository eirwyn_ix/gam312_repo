// Fill out your copyright notice in the Description page of Project Settings.


#include "AIAttackAnimNotify.h"
#include "AIChar.h"

#define DEBUG(Cd, F) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cd, F)


void UAIAttackAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);
	DEBUG(Orange, __FUNCTION__);
	const auto Enemy = MeshComp->GetOwner();
	if (IsValid(MeshComp) && IsValid(Enemy))
	{
		const auto* Attacker = Cast<AAIChar>(Enemy);
		if (IsValid(Attacker))
		{
			Attacker->DealDamage();
		}
	}
}
