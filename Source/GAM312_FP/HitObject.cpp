// Fill out your copyright notice in the Description page of Project Settings.


#include "HitObject.h"
#include "Components/SphereComponent.h"
#include "Interfaces/DamageTaken_Interface.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AHitObject::AHitObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
	PrimaryActorTick.TickInterval = 0.02f;

	// for collision detection.
	constexpr float SphereRadius{ 100 }; // default radius.

	//StaticMeshAsset
	StaticMeshAsset = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Break Object")); // Setting defaults.
	StaticMeshAsset->SetMobility(EComponentMobility::Static);
	StaticMeshAsset->SetCollisionProfileName("NoCollision", false); // default static mesh component & dynamic binding.
	StaticMeshAsset->SetGenerateOverlapEvents(false);
	StaticMeshAsset->SetRelativeLocation(FVector(0.0, 0.0, 0.0));
	StaticMeshAsset->SetRelativeScale3D(FVector(2.0, 2.0, 2.0));

	// turn off the generate overlap event (instead use trigger to generate event)
	RootComponent = StaticMeshAsset; // set up to be the root component.

	//HitSphere
	HitSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Hit Volume"));
	HitSphere->SetMobility(EComponentMobility::Static);
	HitSphere->InitSphereRadius(SphereRadius);
	HitSphere->SetCollisionProfileName("BlockAll", false);
	HitSphere->SetGenerateOverlapEvents(false);
	HitSphere->OnComponentHit.AddDynamic(this, &AHitObject::OnHit);
	HitSphere->SetupAttachment(RootComponent); // attach to the root component.
	HitSphere->SetRelativeLocation(FVector(0.f, 0.f, 0.f)); // offset the z position by SphereRadius/2

 	using FCh = ConstructorHelpers;
	static FCh::FObjectFinder<UStaticMesh> SkullAsset(TEXT("/Game/FX/Skull/Skull_Expolision_mesh.Skull_Expolision_mesh")); // find the mesh asset.
	if (SkullAsset.Succeeded()) // assign the assets to the component (static mesh).
	{
		StaticMeshAsset->SetStaticMesh(SkullAsset.Object);

	}


 // find the animation curve
	static FCh::FObjectFinder<UCurveFloat> SkullCurve(TEXT("/Game/Test_Curve.Test_Curve"));
 if (SkullCurve.Succeeded())
 {
	 FXCurveFloat = SkullCurve.Object; // assign the curve.
 }


 // find hit sound.
	FCh::FObjectFinder<USoundBase> HitSound(TEXT("/Game/FX/Skull/skull_explosion.skull_explosion"));
	if (HitSound.Succeeded())
	{
		SoundWave = HitSound.Object; // assign sound.
	}
}

// Called when the game starts or when spawned
void AHitObject::BeginPlay()
{
	Super::BeginPlay();

	// DYNAMIC MATERIAL SETUP
	Material = StaticMeshAsset->GetMaterial(0); // prep for the dynamic material instance.
	DynamicMaterial = UMaterialInstanceDynamic::Create(Material, this);
	// making a new dynamic material instance from the material above.
	StaticMeshAsset->SetMaterial(0, DynamicMaterial); // set the first material slot to the dynamic material instance.

	// bind timeline progress to function.
	FOnTimelineFloat TimelineProgress; // timeline will update the FXUpdate event.
	TimelineProgress.BindUFunction(this, FName("FXUpdate"));
	FOnTimelineEvent TimelineFinished; // bind FXEnd and  TimelineFinished together.
	TimelineFinished.BindUFunction(this, FName("FXEnd"));

	FXTimeline.AddInterpFloat(FXCurveFloat, TimelineProgress); // add a float interpolation to the float timeline.
	FXTimeline.SetTimelineFinishedFunc(TimelineFinished); // set delegate to call when the timeline is finished.

}

// Called every frame
void AHitObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FXTimeline.TickTimeline(DeltaTime); // time line ticks
}

void AHitObject::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if (IsValid(FXCurveFloat))
	{
		SetActorTickEnabled(true);
		FXTimeline.Play(); // player the timeline.
		HitSphere->DestroyComponent(); // destroy the collision sphere
		GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Green,
			FString::Printf(TEXT("Hitting: %s"), *OtherActor->GetName()), true); // output a message.
		if (IsValid(SoundWave))
		{
			UGameplayStatics::PlaySoundAtLocation(this, SoundWave, GetActorLocation()); // play sound
		}

		// checking the interface implementation.
		FTimerHandle tHandle;
		GetWorldTimerManager().SetTimer(tHandle, [this, OtherActor]()
			{
				//auto dist = GetDistanceTo(OtherActor);
				if (OtherActor->GetClass()->ImplementsInterface(UDamageTaken_Interface::StaticClass()))
					// check if interface is implemented.
				{
					IDamageTaken_Interface::Execute_DamageTaken(OtherActor, 50);
					// passing damage for the actor that has the interface implemented.
				}
			}, .1, false, .5);
	}
}

inline void AHitObject::FXUpdate(float F)
{
	if (IsValid(DynamicMaterial))
	{
		DynamicMaterial->SetScalarParameterValue(TEXT("Display Frame"), F);
		DynamicMaterial->SetScalarParameterValue(TEXT("Emissive Intensity"), 100*(-.00694 * pow(F - 120, 2) + 100));
	}
}

void AHitObject::FXEnd()
{
	GEngine->AddOnScreenDebugMessage(1, 2.5, FColor::Green,
		FString::Printf(TEXT("Animation Ended: hit object")));
	SetActorTickEnabled(true);
	Destroy();
}

