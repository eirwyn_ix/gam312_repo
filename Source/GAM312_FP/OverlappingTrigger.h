// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TimelineComponent.h"
#include "OverlappingTrigger.generated.h"

UCLASS()
class GAM312_FP_API AOverlappingTrigger final : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AOverlappingTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Timeline.
	FTimeline FXTimeline;
	UPROPERTY(EditAnywhere, Category = "Timeline", Transient)
	UCurveFloat* FXCurveFloat;

	// Variables for storing materials later.
	UPROPERTY(Transient)
	UMaterialInterface* Material;
	UPROPERTY(Transient)
	UMaterialInstanceDynamic* DynamicMaterial;

	// add a static mesh (can be set in the editor)
	UPROPERTY(VisibleAnywhere) // expose the static mesh component in the editor.
	UStaticMeshComponent* StaticMeshAsset;
	// add a light sources.
	UPROPERTY(VisibleAnywhere, Category = "Light")
	class USpotLightComponent* SpotLight;
	// declare spherical trigger component.
	UPROPERTY(VisibleAnywhere, Category = "Trigger")
	class USphereComponent* TriggerSphere;
	// declare light intensity variable.
	UPROPERTY(EditAnywhere, Category = "Light")
	float LightIntensity{100000.f};
	// declare the radius for trigger sphere.
	UPROPERTY(EditAnywhere, Category = "Trigger")
	float TriggerRadius{500.f};

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                    const FHitResult& SweepResult);

	// declare overlap end function.
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                  int32 OtherBodyIndex);
	UFUNCTION()
	void FXUpdate(float F);
	UFUNCTION()
	void FXEnd();
};
